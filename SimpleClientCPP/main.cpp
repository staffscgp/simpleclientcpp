#include "SimpleClient.h"

int main()
{
	static const std::string hostname = "127.0.0.1";
	static const std::string port = "4444";

	SimpleClient _client;

	if (_client.Connect(hostname, port))
	{
		std::cout << "Connected..." << std::endl;

		try
		{
			_client.Run();
		}
		catch (NotConnectedException& e)
		{
			std::cout << "Client not Connected" << std::endl;
		}
		catch (boost::system::system_error& e)
		{
			std::cout << "Network Error: " << e.what();
		}
	}
	else
	{
		std::cout << "Failed to connect to: " << hostname << ":" << port;
	}
}